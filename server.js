const express = require('express')
const dotenv = require('dotenv')
const morgan = require('morgan')

const connectDB=require('./src/config/db')
const router=require('./src/routes/user')

dotenv.config({ path: './src/config/config.env' })
//app initialisation
connectDB()

const app = express()

app.use(express.json())

app.use('/api/v1/',router)

const PORT = process.env.PORT || 5000

app.listen(PORT, console.log(`Server is running in ${process.env.NODE_ENV} mode in port ${PORT}`))