const User = require('../models/User')
const Settings = require('../models/Settings')
const UserService = {
    register: async (userDTO) => {
        const user = await User.create(userDTO);
        const settings = await Settings.create({ userId: user._id })// needs userRecord to have the database id 
        return {
            userDetails: user,
            userSettings: settings
        };
    },
    signIn: async (loginData) => {
        try {
            const user = await User.find({ email: loginData.email })
            console.log(`User is ::${user}`)
            var token = jwt.sign(loginData, "rideshare", { algorithm: 'HS256', expiresIn: '1h' });
            return token;
        }
        catch (e) {
            console.log(e)
            throw e;
        }
    },
    getAll: async () => {
        try {
            const users = await User.find()
            return users;
        }
        catch (e) {
            console.log(e)
            throw e;
        }
    }
}

module.exports = UserService