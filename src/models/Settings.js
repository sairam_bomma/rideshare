const mongoose = require('mongoose')

const SettingsSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    rideMatchingGender: {
        type:String, 
        enum:['MALE', 'FEMALE', 'ANY'],
        default: 'ANY'
    },
    maxDistance: {
        type: Number,
        default: 3
    },
})
module.exports = mongoose.model('Settings', SettingsSchema)