const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        trim: true,
        unique: true,
        required: [true, "Email is mandatory"]
    },
    userName: {
        type: String,
        required: [true, "Username is mandatory"]
    },
    phone: {
        type: String,
        required: [true, "Mobile number can't be empty"]
    },
    gender: {
        type: String,
        enum: ['MALE', 'FEMALE'],
        required: [true, "Gender can only be either male or female"]
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})
module.exports = mongoose.model('User', UserSchema)