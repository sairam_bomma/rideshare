const User = require('../models/User')
const Settings = require('../models/Settings')
const UserService=require('../services/UserService')
// @desc get all user Details
// @route GET /api/v1/user
// @access public
exports.getUsers = async (req, res, next) => {
    try {
        const users = await UserService.getAll()
        return res.status(200).json({
            success: true,
            count: users.length,
            data: users
        })
    }
    catch (err) {
        return res.status(500).json({
            success: false,
            error: err
        })
    }
}
exports.registerUser = async (req, res, next) => {
    try {
        const userDTO = req.body;
        const resp = await UserService.register(userDTO);
        return res.status(201).json({
            success: true,
            data: resp
        })
    }
    catch (err) {
        console.log(err)
        return res.status(500).json({
            success: false,
            error: err
        })
    }
}

exports.loginUser = async (req, res, next) => {
    try {
        // const { email, userName } = req.body
        const userDTO=req.body
        const user = await UserService.signIn(userDTO)
        console.log(user)
    }
    catch (err) {


    }
}

exports.updateUser = async (req, res, next) => {
    try {
        const user = await User.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true })
        return res.status(200).json({
            success: true,
        })
    }
    catch (err) {
        console.log(err)
        return res.status(500).json({
            success: false,
            error: err
        })
    }
}