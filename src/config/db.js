const mongoose = require('mongoose');

const connectDB = async () => {
    try {
        const conn = await mongoose.connect('mongodb://localhost:27017/rideshare', { useNewUrlParser: true, useUnifiedTopology: true });
        console.info(`Mongo DB connected and ${conn.connection.host}`)
    } catch (e) {
        console.log(`connection error:: ${e}`);
        process.exit(1);
    }
}
module.exports = connectDB