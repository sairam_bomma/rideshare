const jwt = require('jsonwebtoken')
const privateKey = "rideshare"

const signToken = (req, res, next) => {
    try {

        var token = jwt.sign(req.body,"rideshare", { algorithm: 'HS256', expiresIn: '1h' });
        return res.status(200).json({
            success: true,
            data: token
        })
    }
    catch (e) {
        console.log(e)
        return res.status(400).json({
            success: false,
            error: e
        })
    }

}
const tokenValidator = (req, res, next) => {
    try {
        var decoded = jwt.verify(req.headers.authroization, privateKey,{ algorithms: ['HS256'] });
        console.log(decoded)
        return next()
    }
    catch (e) {
        return res.status(401).json({
            success: true,
            error: e
        })

    }
}
module.exports = {
    tokenValidator,
    signToken
}