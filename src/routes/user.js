const express = require('express')
const router = express.Router()
const { tokenValidator, signToken } = require('../middleware/tokenValidator')
const { registerUser, updateUser, getUsers, loginUser } = require('../controllers/user')

// router.get('/register',(req, res) => res.send("Welcome to ride share"))
router
    .route('/register')
    .post(registerUser)

router
    .route('/login')
    .post(loginUser)

router
    .route('/')
    .get(getUsers)

router
    .route('/updateProfile/:id')
    .patch(tokenValidator, updateUser)

module.exports = router